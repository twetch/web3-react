import { useLayoutEffect } from 'react';
import useWalletProvider from './useWalletProvider.mjs';
export default function useConnectWallet() {
    const { setContext, paymail, publicKey } = useWalletProvider();
    useLayoutEffect(() => {
        if (paymail && publicKey) {
            return;
        }
        setTimeout(() => {
            if (window.bitcoin) {
                window.bitcoin.connect({ onlyIfTrusted: true }).then((r) => {
                    setContext({ paymail: r.paymail, publicKey: r.publicKey });
                });
            }
        }, 100);
    }, [paymail, publicKey, setContext]);
}
//# sourceMappingURL=useConnectIfTrusted.js.map