import { useState, useCallback } from 'react';
export default function useProviderContext() {
    const [globalContextValue, setGlobalContextValue] = useState({
        paymail: '',
        publicKey: ''
    });
    const setContext = useCallback((obj) => {
        setGlobalContextValue({ ...globalContextValue, ...obj });
    }, [globalContextValue]);
    return { ...globalContextValue, setContext };
}
//# sourceMappingURL=useProviderContext.js.map