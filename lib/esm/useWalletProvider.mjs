import { useContext, createContext } from 'react';
export const WalletProvider = createContext({
    paymail: '',
    publicKey: '',
    setContext: (obj) => { }
});
export default function useWalletProvider() {
    const context = useContext(WalletProvider);
    return context;
}
//# sourceMappingURL=useWalletProvider.js.map