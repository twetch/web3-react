"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useConnectIfTrusted = exports.useConnectWallet = exports.useWalletProvider = exports.useProviderContext = exports.WalletProvider = void 0;
const useWalletProvider_1 = __importStar(require("./useWalletProvider"));
exports.useWalletProvider = useWalletProvider_1.default;
Object.defineProperty(exports, "WalletProvider", { enumerable: true, get: function () { return useWalletProvider_1.WalletProvider; } });
const useProviderContext_1 = __importDefault(require("./useProviderContext"));
exports.useProviderContext = useProviderContext_1.default;
const useConnectWallet_1 = __importDefault(require("./useConnectWallet"));
exports.useConnectWallet = useConnectWallet_1.default;
const useConnectIfTrusted_1 = __importDefault(require("./useConnectIfTrusted"));
exports.useConnectIfTrusted = useConnectIfTrusted_1.default;
//# sourceMappingURL=index.js.map