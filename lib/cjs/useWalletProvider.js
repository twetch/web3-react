"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WalletProvider = void 0;
const react_1 = require("react");
exports.WalletProvider = (0, react_1.createContext)({
    paymail: '',
    publicKey: '',
    setContext: (obj) => { }
});
function useWalletProvider() {
    const context = (0, react_1.useContext)(exports.WalletProvider);
    return context;
}
exports.default = useWalletProvider;
//# sourceMappingURL=useWalletProvider.js.map