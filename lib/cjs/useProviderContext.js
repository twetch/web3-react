"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
function useProviderContext() {
    const [globalContextValue, setGlobalContextValue] = (0, react_1.useState)({
        paymail: '',
        publicKey: ''
    });
    const setContext = (0, react_1.useCallback)((obj) => {
        setGlobalContextValue(Object.assign(Object.assign({}, globalContextValue), obj));
    }, [globalContextValue]);
    return Object.assign(Object.assign({}, globalContextValue), { setContext });
}
exports.default = useProviderContext;
//# sourceMappingURL=useProviderContext.js.map