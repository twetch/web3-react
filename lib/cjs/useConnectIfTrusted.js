"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const useWalletProvider_1 = __importDefault(require("./useWalletProvider"));
function useConnectWallet() {
    const { setContext, paymail, publicKey } = (0, useWalletProvider_1.default)();
    (0, react_1.useLayoutEffect)(() => {
        if (paymail && publicKey) {
            return;
        }
        setTimeout(() => {
            if (window.bitcoin) {
                window.bitcoin.connect({ onlyIfTrusted: true }).then((r) => {
                    setContext({ paymail: r.paymail, publicKey: r.publicKey });
                });
            }
        }, 100);
    }, [paymail, publicKey, setContext]);
}
exports.default = useConnectWallet;
//# sourceMappingURL=useConnectIfTrusted.js.map