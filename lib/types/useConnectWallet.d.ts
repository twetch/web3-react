export default function useConnectWallet(): {
    handleConnect: () => Promise<void>;
};
