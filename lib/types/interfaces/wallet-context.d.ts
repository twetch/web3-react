interface WalletContext {
    paymail: string;
    publicKey: string;
}
export default WalletContext;
