/// <reference types="react" />
export declare const WalletProvider: import("react").Context<{
    paymail: string;
    publicKey: string;
    setContext: (obj: any) => void;
}>;
export default function useWalletProvider(): {
    paymail: string;
    publicKey: string;
    setContext: (obj: any) => void;
};
