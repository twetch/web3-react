import WalletContext from './interfaces/wallet-context';
export default function useProviderContext(): {
    setContext: (obj: WalletContext) => void;
    paymail: string;
    publicKey: string;
};
