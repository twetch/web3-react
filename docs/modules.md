[Documentation](README.md) / Exports

# Documentation

## Table of contents

### Variables

- [WalletProvider](modules.md#walletprovider)

### Functions

- [useConnectIfTrusted](modules.md#useconnectiftrusted)
- [useConnectWallet](modules.md#useconnectwallet)
- [useProviderContext](modules.md#useprovidercontext)
- [useWalletProvider](modules.md#usewalletprovider)

## Variables

### WalletProvider

• `Const` **WalletProvider**: `Context`<{ `paymail`: `string` = ''; `publicKey`: `string` = ''; `setContext`: (`obj`: `any`) => `void`  }\>

#### Defined in

[useWalletProvider.ts:3](https://gitlab.com/twetch/web3-react/-/blob/896dc2c/src/useWalletProvider.ts#L3)

## Functions

### useConnectIfTrusted

▸ **useConnectIfTrusted**(): `void`

#### Returns

`void`

___

### useConnectWallet

▸ **useConnectWallet**(): `Object`

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `handleConnect` | () => `Promise`<`void`\> |

___

### useProviderContext

▸ **useProviderContext**(): `Object`

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `paymail` | `string` |
| `publicKey` | `string` |
| `setContext` | (`obj`: `WalletContext`) => `void` |

___

### useWalletProvider

▸ **useWalletProvider**(): `Object`

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `paymail` | `string` |
| `publicKey` | `string` |
| `setContext` | (`obj`: `any`) => `void` |
