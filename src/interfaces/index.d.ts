export {}

interface InjectedBitcoin {
	isTwetch: boolean
	connect: (props?: { onlyIfTrusted?: boolean }) => Promise<{ paymail: string; publicKey: string }>
	abi: (params: any) => Promise<any>
}

declare global {
	interface Window {
		bitcoin: InjectedBitcoin
	}
}
