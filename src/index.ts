import useWalletProvider, { WalletProvider } from './useWalletProvider'
import useProviderContext from './useProviderContext'
import useConnectWallet from './useConnectWallet'
import useConnectIfTrusted from './useConnectIfTrusted'

export {
	WalletProvider,
	useProviderContext,
	useWalletProvider,
	useConnectWallet,
	useConnectIfTrusted
}
