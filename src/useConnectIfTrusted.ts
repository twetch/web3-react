import { useLayoutEffect } from 'react'
import web3 from '@twetch/web3'
import useWalletProvider from './useWalletProvider'

export default function useConnectWallet() {
	const { setContext, paymail, publicKey } = useWalletProvider()

	useLayoutEffect(() => {
		if (paymail && publicKey) {
			return
		}

		setTimeout(() => {
			if (window.bitcoin) {
				window.bitcoin.connect({ onlyIfTrusted: true }).then((r) => {
					setContext({ paymail: r.paymail, publicKey: r.publicKey })
				})
			}
		}, 100)
	}, [paymail, publicKey, setContext])
}
