import { useState, useCallback } from 'react'

import WalletContext from './interfaces/wallet-context'

export default function useProviderContext() {
	const [globalContextValue, setGlobalContextValue] = useState<WalletContext>({
		paymail: '',
		publicKey: ''
	})
	const setContext = useCallback(
		(obj: WalletContext) => {
			setGlobalContextValue({ ...globalContextValue, ...obj })
		},
		[globalContextValue]
	)

	return { ...globalContextValue, setContext }
}
