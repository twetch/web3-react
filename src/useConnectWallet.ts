import { useCallback } from 'react'
import web3 from '@twetch/web3'
import useWalletProvider from './useWalletProvider'

export default function useConnectWallet() {
	const { setContext } = useWalletProvider()
	const handleConnect = useCallback(async () => {
		const response = await web3.connect()
		setContext(response)
	}, [setContext])

	return { handleConnect }
}
