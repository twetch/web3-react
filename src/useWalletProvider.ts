import { useContext, createContext } from 'react'

export const WalletProvider = createContext({
	paymail: '',
	publicKey: '',
	setContext: (obj: any): void => {}
})

export default function useWalletProvider() {
	const context = useContext(WalletProvider)
	return context
}
